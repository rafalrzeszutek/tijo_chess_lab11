package pl.edu.pwsztar.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.dto.PositionDto;

@Component
public class ChessmanMoveMapper implements ChessConverter<PositionDto, FigureMoveDto>{

    @Override
    public PositionDto convert(FigureMoveDto figureMoveDto) {
        String[] start = figureMoveDto.getStart().split("_");
        String[] destination = figureMoveDto.getDestination().split("_");

        int start_x = start[0].charAt(0);
        int start_y = Integer.parseInt(start[1]);
        int destination_x = destination[0].charAt(0);
        int destination_y = Integer.parseInt(destination[1]);

        return new PositionDto(start_x, start_y, destination_x, destination_y);
    }
}
