package pl.edu.pwsztar.mapper;

public interface ChessConverter<T, F> {
    T convert(F from);
}
