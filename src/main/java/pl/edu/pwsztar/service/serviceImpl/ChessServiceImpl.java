package pl.edu.pwsztar.service.serviceImpl;

import org.h2.engine.Right;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.dto.PositionDto;
import pl.edu.pwsztar.mapper.ChessConverter;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame queen;
    private RulesOfGame pawn;
    private RulesOfGame rock;
    private RulesOfGame king;
    private ChessConverter<PositionDto, FigureMoveDto> chessConverter;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Pawn") RulesOfGame pawn,
                            @Qualifier("Rock") RulesOfGame rock,
                            @Qualifier("King") RulesOfGame king,
                            ChessConverter<PositionDto, FigureMoveDto> chessConverter) {
        this.bishop = bishop;
        this.knight = knight;
        this.queen = queen;
        this.pawn = pawn;
        this.rock = rock;
        this.king = king;
        this.chessConverter = chessConverter;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {

        PositionDto positionDto = chessConverter.convert(figureMoveDto);

        switch (figureMoveDto.getType()) {
            case BISHOP:
                return bishop.isCorrectMove(positionDto.getStart_x(), positionDto.getStart_y(), positionDto.getDestination_x(), positionDto.getDestination_y());
            case KNIGHT:
                return knight.isCorrectMove(positionDto.getStart_x(), positionDto.getStart_y(), positionDto.getDestination_x(), positionDto.getDestination_y());
            case QUEEN:
                return queen.isCorrectMove(positionDto.getStart_x(), positionDto.getStart_y(), positionDto.getDestination_x(), positionDto.getDestination_y());
            case PAWN:
                return pawn.isCorrectMove(positionDto.getStart_x(), positionDto.getStart_y(), positionDto.getDestination_x(), positionDto.getDestination_y());
            case ROCK:
                return rock.isCorrectMove(positionDto.getStart_x(), positionDto.getStart_y(), positionDto.getDestination_x(), positionDto.getDestination_y());
            case KING:
                return king.isCorrectMove(positionDto.getStart_x(), positionDto.getStart_y(), positionDto.getDestination_x(), positionDto.getDestination_y());
            default:
                return false;
        }
    }
}
