package pl.edu.pwsztar.domain.dto;

public class PositionDto {
    private int start_x;
    private int destination_x;
    private int start_y;
    private int destination_y;

    public PositionDto(int start_x, int destination_x, int start_y, int destination_y) {
        this.start_x = start_x;
        this.destination_x = destination_x;
        this.start_y = start_y;
        this.destination_y = destination_y;
    }

    public int getStart_x() {
        return start_x;
    }

    public int getDestination_x() {
        return destination_x;
    }

    public int getStart_y() {
        return start_y;
    }

    public int getDestination_y() {
        return destination_y;
    }
}
