package pl.edu.pwsztar.domain.chess;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BishopTest {

    private RulesOfGame bishop = new RulesOfGame.Bishop();

    @Tag("Bishop")
    @ParameterizedTest
    @CsvSource({
            "3, 3,  6,  0",
            "3, 3,  5,  1",
            "3, 3,  4,  4",
            "3, 3,  0,  6",
    })
    void checkCorrectMoveForBishop(int xStart, int yStart, int xStop, int yStop) {
        assertTrue(bishop.isCorrectMove(xStart, yStart, xStop, yStop));
    }

    @ParameterizedTest
    @CsvSource({
            " 3,  3,  3,   0",
            "10, 10, 10,  10"
    })
    void checkIncorrectMoveForBishop(int xStart, int yStart, int xStop, int yStop) {
        assertFalse(bishop.isCorrectMove(xStart, yStart, xStop, yStop));
    }
}
